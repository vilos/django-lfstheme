What is it?
===========

This is the default theme for `LFS`_, an online shop based on Django.

Information
===========

For more information please visit:

* http://pypi.python.org/pypi/django-lfs
* http://www.getlfs.com

Demo shop
=========

A demo shop can be tried here:

* http://demo.getlfs.com
   
Changes
=======

0.2.1  (2009-12-28)
-------------------

* Improved support for vouchers

0.2.0  (2009-10-22)
-------------------

* Added 'Development Status :: 3 - Alpha' to setup.py

* Added support for vouchers

0.1.1  (2009-10-12)
-------------------

* Removed debug code

0.1  (2009-10-11)
-------------------

* Initial public release

.. _`LFS`: http://pypi.python.org/pypi/django-lfs